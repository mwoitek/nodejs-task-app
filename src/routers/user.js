const express = require('express');

const User = require('../models/user');
const auth = require('../middleware/auth');

/** @typedef { import('mongoose').Document } Document */

const router = new express.Router();

// Create
router.post('/users', async (req, res) => {
  /** @type { Document } */
  const user = new User(req.body);

  try {
    await user.save();
    const token = await user.generateAuthToken();
    res.status(201).send({ user, token });
  } catch (error) {
    res.status(400).send(error);
  }
});

// Read
router.get('/users/me', auth, async (req, res) => res.send(req.user));

// Update
router.patch('/users/me', auth, async (req, res) => {
  const userReq = req.body;

  const userKeys = Object.keys(userReq);
  const allowedUserKeys = Object.keys(User.schema.obj);
  const isValidOperation = userKeys.every((key) => allowedUserKeys.includes(key));
  if (!isValidOperation)
    return res.status(400).send({ error: 'An invalid key was used' });

  try {
    const user = req.user;
    userKeys.forEach((key) => (user[key] = userReq[key]));
    await user.save();
    res.send(user);
  } catch (error) {
    res.status(400).send(error);
  }
});

// Delete
router.delete('/users/me', auth, async (req, res) => {
  try {
    const user = req.user;
    await user.remove();
    res.send(user);
  } catch (error) {
    res.status(500).send();
  }
});

// Login
router.post('/users/login', async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findByCredentials(email, password);
    const token = await user.generateAuthToken();
    res.send({ user, token });
  } catch (error) {
    res.status(400).send();
  }
});

// Logout
router.post('/users/logout', auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter((token) => token.token !== req.token);
    await req.user.save();
    res.send();
  } catch (error) {
    res.status(500).send();
  }
});

router.post('/users/logoutAll', auth, async (req, res) => {
  try {
    req.user.tokens = [];
    await req.user.save();
    res.send();
  } catch (error) {
    res.status(500).send();
  }
});

module.exports = router;
