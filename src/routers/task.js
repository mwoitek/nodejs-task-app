const express = require('express');

const Task = require('../models/task');
const isIdValid = require('../db/is_id_valid');
const auth = require('../middleware/auth');

/** @typedef { import('mongoose').Document } Document */

const router = new express.Router();

// Create
router.post('/tasks', auth, async (req, res) => {
  /** @type { Document } */
  const task = new Task({ ...req.body, owner: req.user._id });

  try {
    await task.save();
    res.status(201).send(task);
  } catch (error) {
    res.status(400).send(error);
  }
});

// Read
// GET /tasks/?completed=true
// GET /tasks/?limit=10&skip=20
// GET /tasks/?sortBy=createdAt_desc
router.get('/tasks', auth, async (req, res) => {
  const match = {};
  if (req.query.completed) match.completed = req.query.completed === 'true';

  const sort = {};
  if (req.query.sortBy) {
    const parts = req.query.sortBy.split('_');
    sort[parts[0]] = parts[1] === 'desc' ? -1 : 1;
  }

  try {
    /** @type { Document } */
    const user = req.user;
    await user
      .populate({
        path: 'tasks',
        match,
        options: {
          limit: parseInt(req.query.limit),
          skip: parseInt(req.query.skip),
          sort,
        },
      })
      .execPopulate();
    res.send(user.tasks);
  } catch (error) {
    res.status(500).send();
  }
});

router.get('/tasks/:id', auth, async (req, res) => {
  const _id = req.params.id;
  if (!isIdValid(_id)) return res.status(400).send({ error: 'ID is invalid' });

  try {
    const task = await Task.findOne({ _id, owner: req.user._id });
    if (!task) return res.status(404).send();
    res.send(task);
  } catch (error) {
    res.status(500).send();
  }
});

// Update
router.patch('/tasks/:id', auth, async (req, res) => {
  const _id = req.params.id;
  if (!isIdValid(_id)) return res.status(400).send({ error: 'ID is invalid' });

  const taskReq = req.body;

  const taskKeys = Object.keys(taskReq);
  const allowedTaskKeys = Object.keys(Task.schema.obj);
  const isValidOperation = taskKeys.every((key) => allowedTaskKeys.includes(key));
  if (!isValidOperation)
    return res.status(400).send({ error: 'An invalid key was used' });

  try {
    const task = await Task.findOne({ _id, owner: req.user._id });
    if (!task) return res.status(404).send();

    taskKeys.forEach((key) => (task[key] = taskReq[key]));
    await task.save();
    res.send(task);
  } catch (error) {
    res.status(400).send(error);
  }
});

// Delete
router.delete('/tasks/:id', auth, async (req, res) => {
  const _id = req.params.id;
  if (!isIdValid(_id)) return res.status(400).send({ error: 'ID is invalid' });

  try {
    /** @type { Document } */
    const task = await Task.findOneAndDelete({ _id, owner: req.user._id });
    if (!task) return res.status(404).send();
    res.send(task);
  } catch (error) {
    res.status(500).send();
  }
});

module.exports = router;
