const express = require('express');

require('./db/mongoose');
const userRouter = require('./routers/user');
const taskRouter = require('./routers/task');

const app = express();

// Maintenance Middleware
// app.use((_, res) =>
//   res
//     .status(503)
//     .send({ message: 'The site is under maintenance. Please, try again soon.' })
// );

app.use(express.json());
app.use(userRouter);
app.use(taskRouter);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server is up on port ${port}`);
});
