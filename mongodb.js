// CRUD Operations

const { MongoClient, ObjectId } = require('mongodb');

const connectionUrl = 'mongodb://127.0.0.1:27017';
const databaseName = 'task-manager';

MongoClient.connect(connectionUrl, (error, client) => {
  if (error) return console.log('Unable to connect to database!');

  const db = client.db(databaseName);

  // Inserting a single document
  // db.collection('users').insertOne(
  //   {
  //     name: 'Ed',
  //     age: 75,
  //   },
  //   (error, result) => {
  //     if (error) return console.log('Unable to insert user!');
  //     console.log(result.insertedId);
  //   }
  // );

  // Inserting several documents
  // db.collection('users').insertMany(
  //   [
  //     {
  //       name: 'Patrick',
  //       age: 81,
  //     },
  //     {
  //       name: 'William',
  //       age: 90,
  //     },
  //   ],
  //   (error, result) => {
  //     if (error) return console.log('Unable to insert users!');
  //     console.log(result.insertedIds);
  //   }
  // );

  // Insert 3 tasks into a new collection
  // db.collection('tasks').insertMany(
  //   [
  //     {
  //       description: 'Configure Doom Emacs',
  //       completed: true,
  //     },
  //     {
  //       description: 'Clean room',
  //       completed: false,
  //     },
  //     {
  //       description: 'Read MongoDB documentation',
  //       completed: false,
  //     },
  //   ],
  //   (error, result) => {
  //     if (error) return console.log('Unable to insert tasks!');
  //     console.log(result.insertedIds);
  //   }
  // );

  // Querying documents
  // db.collection('users').findOne({ name: 'Patrick' }, (error, user) => {
  //   if (error) return console.log('Unable to fetch!');
  //   console.log(user);
  // });

  // db.collection('users').findOne(
  //   { _id: new ObjectId('611092a4f8cac8e0635b32dd') },
  //   (error, user) => {
  //     if (error) return console.log('Unable to fetch!');
  //     console.log(user);
  //   }
  // );

  // db.collection('users')
  //   .find({ name: 'Ed' })
  //   .toArray((_, users) => console.log(users));

  // db.collection('users')
  //   .find({ name: 'Ed' })
  //   .count((_, count) => console.log(count));

  // db.collection('tasks').findOne(
  //   { _id: new ObjectId('611096c10f65a6ae1b22c985') },
  //   (_, task) => console.log(task)
  // );

  // db.collection('tasks')
  //   .find({ completed: false })
  //   .toArray((_, tasks) => console.log(tasks));

  // Updating documents

  // Update a single document
  // db.collection('users')
  //   .updateOne(
  //     { _id: new ObjectId('6110a1ad5869a018aefe1aa8') },
  //     { $set: { name: 'Al' } }
  //   )
  //   .then((result) => console.log(result))
  //   .catch((error) => console.log(error));

  // db.collection('users')
  //   .updateOne({ _id: new ObjectId('6110a1ad5869a018aefe1aa8') }, { $inc: { age: 1 } })
  //   .then((result) => console.log(result))
  //   .catch((error) => console.log(error));

  // Update several documents
  // db.collection('tasks')
  //   .updateMany({ completed: false }, { $set: { completed: true } })
  //   .then((result) => console.log(result))
  //   .catch((error) => console.log(error));

  // Deleting documents

  // Delete several documents
  db.collection('users')
    .deleteMany({ age: { $gt: 80 } })
    .then((result) => console.log(result))
    .catch((error) => console.log(error));

  // Delete a single document
  db.collection('tasks')
    .deleteOne({ description: 'Configure Doom Emacs' })
    .then((result) => console.log(result))
    .catch((error) => console.log(error));
});
